#!/usr/bin/env python
# coding: utf-8

# # Indian Liver Patient Analysis using Logistic Regression with converting variables
# #By- Aarush Kumar
# #Dated: September 16,2021

# ### About

# Liver disease is a growing problem of our time, and having a good method to identify the patients most at risk could help doctors make a faster diagnosis and treatment. We must remember that statistical methods are intended to HELP diagnosticians, not replace them as unquestionable oracles.
# * Note: During the analysis, certain variables may be removed for the sake of the model. I will use the knowledge acquired from the books with Logistic Regression as the default classification method in my mind.

# Major variables:
# * Bilirubin is a bile pigment that comes from the breakdown of red blood cells. An increase in this concentration may cause jaundice.
# * Alkaline is en enzyme which can by found in the liver and when liver is damaged Alkaline may leak into the bloodstream. Its high levels in blood can indicate liver disease.
# * Alamine Aminotransferase: test result can range from 7 to 55 units per liter.
# * Aspartate_Aminotransferase: normal ranges are: 10-40 units/L (males), 9-32 units/L (females).
# * In people with badly damaged livers, proteins are not properly processed.
# * Low albumin levels can indicate a problem with liver or kidneys.
# * Globulins play an important role in liver function, blood clotting, and fighting infection. Low globulin levels can be a sign of liver or kidney disease. High levels may indicate infection, inflammatory disease or immune disorders.

# In[1]:


get_ipython().system('pip install factor_analyzer')


# ## Factor Analysis in Machine Learning :
# 1. Reduce a large numbers of variables into fewer numbers of factors.
# 2. Puts maximum common variance into a common score.
# 3. Associates multiple observed variables with a latent variable.
# 4. Has the same numbers of factors and variables,where each factor contains a certain amount of overall variance .

# In[2]:


import pandas as pd
import matplotlib as plt
import seaborn as sns 
import numpy as np
from scipy.stats import kurtosis, skew, shapiro, zscore
from factor_analyzer.factor_analyzer import calculate_bartlett_sphericity, calculate_kmo
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import plot_confusion_matrix, confusion_matrix, accuracy_score, precision_score, recall_score, balanced_accuracy_score, roc_curve, roc_auc_score


# In[3]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Indian Liver Patient Logistic Regression/indian_liver_patient.csv")


# In[4]:


df


# In[5]:


df.info()


# In[6]:


df.isnull().sum()


# In[7]:


df.shape


# In[8]:


df.size


# In[9]:


df.columns


# In[10]:


df.dtypes


# In[11]:


df.describe()


# In[12]:


df[df.isna().any(axis=1)]


# In[13]:


df = df.dropna()


# In[14]:


df_c = df.copy()


# In[15]:


df_c["Dataset"] = df["Dataset"].map({1:"Sick", 2:"Healthy"})


# In[16]:


df['Gender'] = df['Gender'].map({'Male': 1, 'Female': 0})
df['Dataset'] = df['Dataset'].map({1: 1, 2: 0})
df.rename(columns={'Gender': 'Male'}, inplace=True)
df.rename(columns={'Dataset': 'Target'}, inplace=True)


# ## Dividing dataset into categorical and quantitative variables

# In[17]:


df_c.drop(columns=['Age', 'Total_Bilirubin', 'Direct_Bilirubin',
                   'Alkaline_Phosphotase', 'Alamine_Aminotransferase',
                   'Aspartate_Aminotransferase', 'Total_Protiens', 'Albumin',
                   'Albumin_and_Globulin_Ratio'], inplace=True)


# In[18]:


df_quantitative = df.drop(columns=["Male", "Target"])


# ## Operation on categorical variables

# In[19]:


df_c.Gender.value_counts(normalize=True)


# In[20]:


df_c.Dataset.value_counts(normalize=True)


# In[21]:


df_c.groupby("Dataset").Gender.value_counts()


# In[22]:


pd.crosstab(df_c['Gender'], df_c['Dataset']).apply(lambda r: r/r.sum()*100, axis=1)


# In[23]:


plt.rcParams['figure.figsize'] = [10, 8]  # for size
sns.countplot(x="Gender", hue="Dataset", data=df_c).set_title("Liver dieses among Gender")


# ## Operation on quantitative variables

# In[24]:


df_quantitative.describe()


# In[25]:


# coefficient of variation
def cv(x): return np.std(x) / np.mean(x) * 100
df_quantitative.apply(cv)


# In[26]:


# kurtosis
df_quantitative.apply(kurtosis, bias=False)


# Kurtosis is a measure of outliers. The higher its value, the more likely there are outliers in the database. The lower the value, the more the results are clustered around the mean.

# In[27]:


# skewness
df_quantitative.apply(skew, bias=False)


# The skewness for most variables is positive, indicating that the distribution has an extended right arm.

# In[28]:


for i in df_quantitative:
    print([i])
    a, b = shapiro(df_quantitative[[i]])
    if b < 0.05:
        print("H1")
    else:
        print("H0")


# ### alpha = 0.05
# ### H0 = The sample comes from a normal distribution.
# ### H1 = The sample is not coming from a normal distribution.

# ## Outleiers

# In[29]:


sns.boxplot(data=df_quantitative, orient="h").set_title("Plot showing outliers")


# To find outliers we will use the interquartile range IQR=Q3−Q1. The outlier observations are below the lower bound defined as lb=Q1−1.5∗IQR and above the upper bound defined as ub=Q3+1.5∗IQR.

# In[30]:


def remove_outliers(df_in):
    Q1 = df_in.quantile(0.25)
    Q3 = df_in.quantile(0.75)
    IQR = Q3 - Q1
    upper_limit = Q3 + 1.5*IQR
    lower_limit = Q1 - 1.5*IQR
    df_clean = df_in[~((df_in < lower_limit) | (df_in > upper_limit)).any(axis=1)]
    return df_clean


# In[31]:


df_clean = remove_outliers(df_quantitative)


# In[32]:


sns.boxplot(data=df_clean, orient="h").set_title(
    "Plot showing outliers after the 1st removal of outliers")


# In[33]:


for i in range(5):
    df_clean = remove_outliers(df_clean)


# In[34]:


sns.boxplot(data=df_clean, orient="h").set_title(
    "Plot showing outliers after the 6th removal of outliers")


# In[35]:


print("Number of cases in df:", len(df))
print("Number of cases in df_clean:", len(df_clean))
print("We've removed:", round(100-(len(df_clean)*100/len(df)),2), "percent of rows.")


# In[36]:


df_c_trimmed = df_c[df_c.index.isin(df_clean.index)]


# In[37]:


df_c_trimmed.Dataset.value_counts()


# In[38]:


df_c_trimmed.Gender.value_counts()


# In[39]:


df_trimmed = df[df.index.isin(df_clean.index)]


# In[40]:


sns.heatmap(df_quantitative.corr(), annot=True, cmap='coolwarm',
            mask=np.triu(df_quantitative.corr())).set_title("Before removing outliers")


# In[41]:


sns.heatmap(df_clean.corr(), annot=True, cmap='coolwarm',
            mask=np.triu(df_clean.corr())).set_title("After removing outliers")


# ## Logistic Regression

# In[42]:


df_trimmed.drop(columns="Albumin", inplace=True)


# ## Splitting data to X & y

# In[47]:


X = df_trimmed.loc[:, df_trimmed.columns!='Target']
y = df_trimmed.loc[:, 'Target']


# In[48]:


X_all = df.loc[:, df.columns!='Target']
y_all = df.loc[:, 'Target']


# In[49]:


X_train_all, X_test_all, y_train_all, y_test_all = train_test_split(X_all, y_all, test_size = 0.30, random_state = 0, stratify = y_all)


# ## Model

# In[50]:


model = LogisticRegression(max_iter=1000)


# In[51]:


res_1 = model.fit(X, y)
y_predict_1 = model.predict(X)
confusion_matrix(y_pred=y_predict_1,y_true=y)


# In[52]:


print("Accuracy:", accuracy_score(y, y_predict_1))
print("Precision:", precision_score(y, y_predict_1))
print("Recall:", recall_score(y, y_predict_1))
print("Balanced accuracy score:", balanced_accuracy_score(y, y_predict_1))


# In[53]:


logit_roc_auc_1 = roc_auc_score(y, y_predict_1)
fpr_1, tpr_1, thresholds_1 = roc_curve(y, res_1.predict_proba(X)[:, 1])
plt.pyplot.plot(fpr_1, tpr_1, label='Logistic Regression (area = %0.2f)' % logit_roc_auc_1)
plt.pyplot.plot([0, 1], [0, 1], 'r--')
plt.pyplot.xlim([0.0, 1.0])
plt.pyplot.ylim([0.0, 1.05])
plt.pyplot.xlabel('False Positive Rate')
plt.pyplot.ylabel('True Positive Rate')
plt.pyplot.title('Receiver operating characteristic for df')
plt.pyplot.legend(loc="lower right")


# In[54]:


res_2 = model.fit(X_train_all, y_train_all)
y_predict_2 = model.predict(X_test_all)
confusion_matrix(y_pred=y_predict_2, y_true=y_test_all)


# In[55]:


print("Accuracy:", accuracy_score(y_test_all, y_predict_2))
print("Precision:", precision_score(y_test_all, y_predict_2))
print("Recall:", recall_score(y_test_all, y_predict_2))
print("Balanced accuracy score:", balanced_accuracy_score(y_test_all, y_predict_2))


# In[56]:


logit_roc_auc_2 = roc_auc_score(y_test_all, y_predict_2)
fpr_2, tpr_2, thresholds_2 = roc_curve(y_test_all, res_2.predict_proba(X_test_all)[:, 1])
plt.pyplot.plot(fpr_2, tpr_2, label='Logistic Regression (area = %0.2f)' % logit_roc_auc_2)
plt.pyplot.plot([0, 1], [0, 1], 'r--')
plt.pyplot.xlim([0.0, 1.0])
plt.pyplot.ylim([0.0, 1.05])
plt.pyplot.xlabel('False Positive Rate')
plt.pyplot.ylabel('True Positive Rate')
plt.pyplot.title('Receiver operating characteristic for df_all')
plt.pyplot.legend(loc="lower right")


# ## Comparison of results

# In[57]:


data = {"df_trimmed": [accuracy_score(y, y_predict_1), precision_score(y, y_predict_1), recall_score(y, y_predict_1), balanced_accuracy_score(y, y_predict_1)],
        "df": [accuracy_score(y_test_all, y_predict_2), precision_score(y_test_all, y_predict_2), recall_score(y_test_all, y_predict_2), balanced_accuracy_score(y_test_all, y_predict_2)]}

comparision = pd.DataFrame(data, index = ["Accuracy", "Precision", "Recall", "Balanced accuracy"])
print(comparision)


# In[58]:


print("Liver patients percentage in df_trimmed:", df_trimmed.Target.sum()/len(df_trimmed.Target))
print("Liver patients percentage in df:", df.Target.sum()/len(df.Target))


# ## Conclusion

# * The first conclusion we can make is: DELETING almost 80% of our database because of outliers is a bad idea. We really shouldn't be doing this.
# * The models for the two cases are slightly different say about 10% on average. Which means that laboriously checking assumptions, eliminating outliers, removing a highly correlated variable, etc. produced a poor end result.
# * Of course, it should be noted that in our decimated database, about 51% of cases had a diseased liver, and our accuracy is 61% in this model. Using this model, we slightly improve our assessment of whether a patient has a diseased liver or not, compared to assuming that all patients have the disease. For the entire database, patients with diseased liver make up about 72% of the cases, and our model has a accuracy of 70%, so just looking at this rate, you could say that whether we use the model or assume that everyone has the disease...it doesn't matter.
